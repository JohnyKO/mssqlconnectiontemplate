#include "MSSQLConnection.h"
#include "UsersContext.h"

using namespace System::Data;
using namespace System::Data::SqlClient;

//������ ����������� � ���� �� ������� ���� ������
//Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=ConsoleUsersDB;Integrated Security=True
int main()
{
	//��������� �����������
	String^ host = "(localdb)\\MSSQLLocalDB";
	String^ dbName = "ConsoleUsersDB";

	//�������� ������� �����������
	MSSQL::Connection^ connect = gcnew MSSQL::Connection(host, dbName);
	
	//������� �����������
	if (connect->startConnection())
	{
		//������� ����� �� ������� � ������� ������
		// ������� ������������ System::String
		Console::WriteLine("Is Connected!");
	}
	else
	{
		//������� ����� �� ������� � ������� ������
		// ������� ������������ System::String
		Console::WriteLine("Is not Connected!");
		return 0;
	}

	//�������� ������� ���������
	//� ������� ����� ��������� �� �������� � �������� � ��
	//�� �������� ������ � ������� connection to Data Base
	ConsoleUsersDB::UserContext^ users = 
		gcnew ConsoleUsersDB::UserContext(connect->getConnectionObject());

	//������ ����� ������ ������������
	ConsoleUsersDB::User user(0, "Dima", "654321");
	
	//������� �������� ��� � ����
	if (true)//users->Insert(user))
	{
		//������� ����� �� ������� � ������� ������
		// ������� ������������ System::String
		Console::WriteLine("User was added!");
	}
	else
	{
		//������� ����� �� ������� � ������� ������
		// ������� ������������ System::String
		Console::WriteLine("User was not added!");
	}
	//New Line
	Console::WriteLine();

	//�������� ��������� ������� SELECT * FROM Users
	// � ���������� ��� � reader, ��� �� ������ ��������� ���
	SqlDataReader^ reader = users->getAllUsers();

	//���� � ���������� ���-�� ����
	if (reader != nullptr)
	{
		//�������� ���-�� �������� � ����������
		//���-�� ����� �� �������� �� ������
		int numberCol = reader->FieldCount;

		//������ ����� �������
		//������� ��� ������ � ������ �� ����� � �����
		//��������� ���-�� �������, ������ ������� ������� numberCol
		for (int i = 0; i < numberCol; i++)
		{
			Console::Write(reader->GetName(i)->ToString() + "\t");
		}

		//������� �� ����� ������
		Console::WriteLine();

		//������ ���� �� reader � ������� ����� �������
		//���� ���� ��� ������
		//���� �������� ������ ���������� �������
		while (reader->Read())
		{
			//reader �� ������ ������� ����� while -
			// ��� ������ �������� ����� ������ ������
			// � ��� ������������������, � ������� �� �� ������� � �������

			//������ � ����� �������� ����������
			for (int i = 0; i < numberCol; i++)
			{
				Console::Write(reader[i]->ToString() + "\t");
			}

			//������� �� ����� ������ ��� ��� ������ �������
			Console::WriteLine();
		}
	}
	else
	{
		Console::WriteLine("����������� ������� ���!");
	}

	//�������� ���������� ��������� �� ������� Enter
	Console::ReadLine();

	return 0;
}