#ifndef  _MSSQL_CONNECTION_
#define _MSSQL_CONNECTION_

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace Data;
using namespace Data::SqlClient;

namespace MSSQL
{
	//����� ������ ��� ����������� � ��
	ref class Connection
	{
		private: String^ host; //����� ����
		private: String^ dbName; //��� ����

		private: SqlConnection^ connection; //������ �����������
		//������� ����������� ������ �����������
		private: SqlConnectionStringBuilder^ stringBuilder;

		public: Connection(String^ host, String^ dbName)
		{
			//������ ���, ��� �� ������ �����
			//�.�. ������ ���� NULL, �� ������ �� ������������ ���...
			connection = nullptr;

			//� ���� ������(�����������) �� ������� ��������� �...
			// ...������� ������ ����������� � ��
			this->host = host;
			this->dbName = dbName;

			stringBuilder = gcnew SqlConnectionStringBuilder();
			stringBuilder->DataSource = host;
			stringBuilder->InitialCatalog = dbName;
			stringBuilder->IntegratedSecurity = true;
		}

		public: bool startConnection()
		{
			//��������� �����������
			bool connected = false;
			try {
				//�������� ����������� � ���� ������
				connection = gcnew SqlConnection(stringBuilder->ToString());
				connection->Open();
				
				//���� �� ����� ����, ������ ��� �� � �� ���������� true
				connected = true;
				return connected;
			}
			catch (...)
			{
				//���� �� ������ ����, ������ ����������� �� �����
				//���������� false
				return connected;
			}
		}

		//����� �������� �����������, ������� ���� �������
		public: SqlConnection^ getConnectionObject()
		{
			return connection;
		}
		
		//������� ����������� � ��
		//������������ ������ ���, ����� ����������� ������ �� �����
		public: bool stopConnection()
		{
			//��������� ���������� 
			bool disconnected = false;
			try {
				//�������� ����������
				connection->Close();

				//���� ��� �� - ���������� true
				disconnected = true;
				return disconnected;
			}
			catch (...)
			{
				//���� ������ ����, ������ �� �����-�� �������
				// ���������� �� ����������
				//������ ����� ������ ����������� � �� ���� � ��� NULL
				return disconnected;
			}
		}
	};
}

#endif // ! _MSSQL_CONNECTION_

