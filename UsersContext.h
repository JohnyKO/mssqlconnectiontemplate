#ifndef _USERS_CONTEXT_
#define _USERS_CONTEXT_

using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace Data;
using namespace Data::SqlClient;

namespace ConsoleUsersDB
{
	//������ ������� ����� 
	// ��� �������� ������ � ������������
	ref class User
	{
		public: int Id;
		public: String^ Login;
		public: String^ Password;

		//��� ������� ������������
		//������ ������
		public: User()
		{
			Id = 0;
			Login = String::Empty;
			Password = String::Empty;
		}

		//��� �������� ���������� ����� � ������
		// (����� ��� Insert, ��� ��� �� ����� Id...
		// ...�.�. Id ��������� ������������� ��� ����������)
		public: User(String^ login, String^ password)
		{
			Id = 0;
			Login = login;
			Password = password;
		}

		//������ �����������
		//�������� ���� ����� �������� � Delete or Update or Select
		// ��� ����� ������������ ��������� ID
		// ������� �� ���-�� ��� ������� �����
		public: User(int id, String^ login, String^ password)
		{
			Id = id;
			Login = login;
			Password = password;
		}
	};

	ref class UserContext
	{
		//������ ����������� � ���� ������
		private: SqlConnection^ connection;

		//������� ����������� ��� ������������� ������� �����������
		public: UserContext(SqlConnection^ connection)
		{
			this->connection = connection;
		}

		//���������� � ���� ������������ �� ����������
		// ������� �������� � ������� user
		public: bool Insert(User% user)
		{
			//������� ��������� ���������
			bool its_ok = false;
			try {
				String^ query = "INSERT INTO dbo.Users(Users.Login, Users.Password) " +
					"VALUES(@Login, @Password)";
				SqlCommand^ command = gcnew SqlCommand(query, connection);
				command->Parameters->Add("@Login", user.Login);
				command->Parameters->Add("@Password", user.Password);

				//���� ��� ��, �� �� ������� �������� true
				command->ExecuteNonQuery();
				its_ok = true;
				return its_ok;
			}
			catch (...)
			{
				//�� ���� �� ������ ����, ������ ���-�� ����� �� ��� :)
				//� �� ������ false
				return its_ok;
			}
		}

		//��������� ���������� ������ ���� �������������
		public: SqlDataReader^ getAllUsers()
		{
			//������� ���������� ��� ���������� �������
			SqlDataReader^ resultReader = nullptr;

			try {
				//������ ������� � ���������� �������
				String^ query = "SELECT * FROM Users;";
				SqlCommand^ command = gcnew SqlCommand(query, connection);

				//��������� ������ � ��������� ��������� � resultReader
				resultReader = command->ExecuteReader();
				
				//���������� ���������� � ����������� �������
				return resultReader;
			}
			catch (...)
			{
				//���� �� ������ ����, ������ ������ �� ����������
				//resultReader �������� NULL
				// -> ���������� ����������� NULL
				return resultReader;
			}
		}		
	};
}

#endif